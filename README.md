This is an attempt to reproduce builds generated in the CI for
https://gitlab.torproject.org/tpo/core/debian/tor.git.

It works by

* getting the metadata from the specified run of the pipeline
(currently hardcoded to [38207](https://gitlab.torproject.org/tpo/core/debian/tor/-/pipelines/38207)
* checking out the at the commit that pipeline ran at
* munging the CI yml into shell scripts
* feeding those scripts into Docker

Example:

```
./reproduce_pipeline.py
...
Done!
Our artifacts: reproduce-38207/ours
Their artifacts: reproduce-38207/theirs
Try diffoscope to compare
```

Comparing the whole directories just tells us that we produced different file
names, since those names include the date.  (To be fixed).

If we compare one of the specific apks, we can see there are some minor differences inside too, though
no differences in the executables.

```
$ diffoscope reproduce-38207/theirs/binary-packages/debian-amd64-bullseye/tor_*bullseye+1_amd64.deb reproduce-38207/ours/binary-packages/debian-amd64-bullseye/tor_*bullseye+1_amd64.deb
--- reproduce-38207/theirs/binary-packages/debian-amd64-bullseye/tor_0.4.6.10-dev-20220509T161328Z-1~d11.bullseye+1_amd64.deb
+++ reproduce-38207/ours/binary-packages/debian-amd64-bullseye/tor_0.4.6.10-dev-20220527T212004Z-1~d11.bullseye+1_amd64.deb
├── file list
│ @@ -1,3 +1,3 @@
│ --rw-r--r--   0        0        0        4 2022-05-09 16:14:16.000000 debian-binary
│ --rw-r--r--   0        0        0     5564 2022-05-09 16:14:16.000000 control.tar.xz
│ --rw-r--r--   0        0        0  1979012 2022-05-09 16:14:16.000000 data.tar.xz
│ +-rw-r--r--   0        0        0        4 2022-05-27 21:20:55.000000 debian-binary
│ +-rw-r--r--   0        0        0     5564 2022-05-27 21:20:55.000000 control.tar.xz
│ +-rw-r--r--   0        0        0  1979508 2022-05-27 21:20:55.000000 data.tar.xz
├── control.tar.xz
│ ├── control.tar
│ │ ├── file list
│ │ │ @@ -1,8 +1,8 @@
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./
│ │ │ --rw-r--r--   0 root         (0) root         (0)      214 2022-05-09 16:14:16.000000 ./conffiles
│ │ │ --rw-r--r--   0 root         (0) root         (0)     2258 2022-05-09 16:14:16.000000 ./control
│ │ │ --rw-r--r--   0 root         (0) root         (0)     2159 2022-05-09 16:14:16.000000 ./md5sums
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)     7272 2022-05-09 16:14:16.000000 ./postinst
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)     1999 2022-05-09 16:14:16.000000 ./postrm
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      187 2022-05-09 16:14:16.000000 ./preinst
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      603 2022-05-09 16:14:16.000000 ./prerm
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      214 2022-05-27 21:20:55.000000 ./conffiles
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     2258 2022-05-27 21:20:55.000000 ./control
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     2159 2022-05-27 21:20:55.000000 ./md5sums
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)     7272 2022-05-27 21:20:55.000000 ./postinst
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)     1999 2022-05-27 21:20:55.000000 ./postrm
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      187 2022-05-27 21:20:55.000000 ./preinst
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      603 2022-05-27 21:20:55.000000 ./prerm
│ │ ├── ./control
│ │ │ @@ -1,9 +1,9 @@
│ │ │  Package: tor
│ │ │ -Version: 0.4.6.10-dev-20220509T161328Z-1~d11.bullseye+1
│ │ │ +Version: 0.4.6.10-dev-20220527T212004Z-1~d11.bullseye+1
│ │ │  Architecture: amd64
│ │ │  Maintainer: Peter Palfrader <weasel@debian.org>
│ │ │  Installed-Size: 5512
│ │ │  Depends: libc6 (>= 2.29), libcap2 (>= 1:2.10), libevent-2.1-7 (>= 2.1.8-stable), liblzma5 (>= 5.1.1alpha+20120614), libseccomp2 (>= 0.0.0~20120605), libssl1.1 (>= 1.1.1), libsystemd0, libzstd1 (>= 1.4.0), zlib1g (>= 1:1.1.4), adduser, runit-helper (>= 2.10.0~), lsb-base
│ │ │  Recommends: logrotate, tor-geoipdb, torsocks
│ │ │  Suggests: mixmaster, torbrowser-launcher, socat, apparmor-utils, nyx, obfs4proxy
│ │ │  Conflicts: libssl0.9.8 (<< 0.9.8g-9)
│ │ ├── ./md5sums
│ │ │ ├── ./md5sums
│ │ │ │┄ Files differ
├── data.tar.xz
│ ├── data.tar
│ │ ├── file list
│ │ │ @@ -1,86 +1,86 @@
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/apparmor.d/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/apparmor.d/abstractions/
│ │ │ --rw-r--r--   0 root         (0) root         (0)      581 2022-05-09 16:14:16.000000 ./etc/apparmor.d/abstractions/tor
│ │ │ --rw-r--r--   0 root         (0) root         (0)      684 2022-05-09 16:14:16.000000 ./etc/apparmor.d/system_tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/cron.weekly/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      322 2022-05-09 16:14:16.000000 ./etc/cron.weekly/tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/default/
│ │ │ --rw-r--r--   0 root         (0) root         (0)     2623 2022-05-09 16:14:16.000000 ./etc/default/tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/init.d/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)     6072 2022-05-09 16:14:16.000000 ./etc/init.d/tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/logrotate.d/
│ │ │ --rw-r--r--   0 root         (0) root         (0)      242 2022-05-09 16:14:16.000000 ./etc/logrotate.d/tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/runit/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/runit/runsvdir/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/runit/runsvdir/default/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/.meta/
│ │ │ --rw-r--r--   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/.meta/installed
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/log/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      140 2022-05-09 16:14:16.000000 ./etc/sv/tor/log/run
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      962 2022-05-09 16:14:16.000000 ./etc/sv/tor/run
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/tor/
│ │ │ --rw-r--r--   0 root         (0) root         (0)     8888 2022-05-09 16:14:16.000000 ./etc/tor/torrc
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./lib/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./lib/systemd/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./lib/systemd/system/
│ │ │ --rw-r--r--   0 root         (0) root         (0)      312 2022-05-09 16:14:16.000000 ./lib/systemd/system/tor.service
│ │ │ --rw-r--r--   0 root         (0) root         (0)     1286 2022-05-09 16:14:16.000000 ./lib/systemd/system/tor@.service
│ │ │ --rw-r--r--   0 root         (0) root         (0)     1061 2022-05-09 16:14:16.000000 ./lib/systemd/system/tor@default.service
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./lib/systemd/system-generators/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)      751 2022-05-09 16:14:16.000000 ./lib/systemd/system-generators/tor-generator
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/bin/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)  3347176 2022-05-09 16:14:16.000000 ./usr/bin/tor
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)   541848 2022-05-09 16:14:16.000000 ./usr/bin/tor-gencert
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)   463992 2022-05-09 16:14:16.000000 ./usr/bin/tor-print-ed-signing-cert
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)   178360 2022-05-09 16:14:16.000000 ./usr/bin/tor-resolve
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)     1375 2022-05-09 16:13:27.000000 ./usr/bin/torify
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/sbin/
│ │ │ --rwxr-xr-x   0 root         (0) root         (0)     2643 2022-05-09 16:14:16.000000 ./usr/sbin/tor-instance-create
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/doc/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/
│ │ │ --rw-r--r--   0 root         (0) root         (0)      460 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/NEWS.Debian.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)     2964 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/README.Debian
│ │ │ --rw-r--r--   0 root         (0) root         (0)    30394 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/changelog.Debian.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)   520345 2022-05-09 16:13:28.000000 ./usr/share/doc/tor/changelog.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)    11342 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/copyright
│ │ │ --rw-r--r--   0 root         (0) root         (0)    17557 2022-05-09 16:13:28.000000 ./usr/share/doc/tor/tor-exit-notice.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)    20874 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/tor-gencert.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)    17821 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/tor-print-ed-signing-cert.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)    18708 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/tor-resolve.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)   280427 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/tor.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)    17798 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/torify.html
│ │ │ --rw-r--r--   0 root         (0) root         (0)     4696 2022-05-09 16:14:16.000000 ./usr/share/doc/tor/torrc.sample.gz
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/lintian/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/lintian/overrides/
│ │ │ --rw-r--r--   0 root         (0) root         (0)       46 2022-05-09 16:14:16.000000 ./usr/share/lintian/overrides/tor
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/man/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/man/man1/
│ │ │ --rw-r--r--   0 root         (0) root         (0)     1645 2022-05-09 16:14:16.000000 ./usr/share/man/man1/tor-gencert.1.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)      816 2022-05-09 16:14:16.000000 ./usr/share/man/man1/tor-print-ed-signing-cert.1.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)     1012 2022-05-09 16:14:16.000000 ./usr/share/man/man1/tor-resolve.1.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)    54069 2022-05-09 16:14:16.000000 ./usr/share/man/man1/tor.1.gz
│ │ │ --rw-r--r--   0 root         (0) root         (0)      763 2022-05-09 16:14:16.000000 ./usr/share/man/man1/torify.1.gz
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/man/man5/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/man/man8/
│ │ │ --rw-r--r--   0 root         (0) root         (0)     1222 2022-05-09 16:14:16.000000 ./usr/share/man/man8/tor-instance-create.8.gz
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/runit/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/runit/meta/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/runit/meta/tor/
│ │ │ --rw-r--r--   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/runit/meta/tor/installed
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/tor/
│ │ │ --rw-r--r--   0 root         (0) root         (0)      351 2022-05-09 16:14:16.000000 ./usr/share/tor/tor-service-defaults-torrc
│ │ │ --rw-r--r--   0 root         (0) root         (0)      431 2022-05-09 16:14:16.000000 ./usr/share/tor/tor-service-defaults-torrc-instances
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./var/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./var/log/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./var/log/runit/
│ │ │ -drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./var/log/runit/tor/
│ │ │ -lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/log/supervise -> /run/runit/supervise/tor.log
│ │ │ -lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./etc/sv/tor/supervise -> /run/runit/supervise/tor
│ │ │ -lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/sbin/tor -> ../bin/tor
│ │ │ -lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-09 16:14:16.000000 ./usr/share/man/man5/torrc.5.gz -> ../man1/tor.1.gz
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/apparmor.d/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/apparmor.d/abstractions/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      581 2022-05-27 21:20:55.000000 ./etc/apparmor.d/abstractions/tor
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      684 2022-05-27 21:20:55.000000 ./etc/apparmor.d/system_tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/cron.weekly/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      322 2022-05-27 21:20:55.000000 ./etc/cron.weekly/tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/default/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     2623 2022-05-27 21:20:55.000000 ./etc/default/tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/init.d/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)     6072 2022-05-27 21:20:55.000000 ./etc/init.d/tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/logrotate.d/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      242 2022-05-27 21:20:55.000000 ./etc/logrotate.d/tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/runit/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/runit/runsvdir/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/runit/runsvdir/default/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/.meta/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/.meta/installed
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/log/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      140 2022-05-27 21:20:55.000000 ./etc/sv/tor/log/run
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      962 2022-05-27 21:20:55.000000 ./etc/sv/tor/run
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/tor/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     8888 2022-05-27 21:20:55.000000 ./etc/tor/torrc
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./lib/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./lib/systemd/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./lib/systemd/system/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      312 2022-05-27 21:20:55.000000 ./lib/systemd/system/tor.service
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     1286 2022-05-27 21:20:55.000000 ./lib/systemd/system/tor@.service
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     1061 2022-05-27 21:20:55.000000 ./lib/systemd/system/tor@default.service
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./lib/systemd/system-generators/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)      751 2022-05-27 21:20:55.000000 ./lib/systemd/system-generators/tor-generator
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/bin/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)  3347176 2022-05-27 21:20:55.000000 ./usr/bin/tor
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)   541848 2022-05-27 21:20:55.000000 ./usr/bin/tor-gencert
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)   463992 2022-05-27 21:20:55.000000 ./usr/bin/tor-print-ed-signing-cert
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)   178360 2022-05-27 21:20:55.000000 ./usr/bin/tor-resolve
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)     1375 2022-05-27 21:20:04.000000 ./usr/bin/torify
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/sbin/
│ │ │ +-rwxr-xr-x   0 root         (0) root         (0)     2643 2022-05-27 21:20:55.000000 ./usr/sbin/tor-instance-create
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/doc/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      460 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/NEWS.Debian.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     2964 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/README.Debian
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    30375 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/changelog.Debian.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)   520345 2022-05-27 21:20:04.000000 ./usr/share/doc/tor/changelog.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    11342 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/copyright
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    17557 2022-05-27 21:20:04.000000 ./usr/share/doc/tor/tor-exit-notice.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    20874 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/tor-gencert.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    17821 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/tor-print-ed-signing-cert.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    18708 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/tor-resolve.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)   280427 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/tor.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    17798 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/torify.html
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     4696 2022-05-27 21:20:55.000000 ./usr/share/doc/tor/torrc.sample.gz
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/lintian/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/lintian/overrides/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)       46 2022-05-27 21:20:55.000000 ./usr/share/lintian/overrides/tor
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/man/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/man/man1/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     1645 2022-05-27 21:20:55.000000 ./usr/share/man/man1/tor-gencert.1.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      816 2022-05-27 21:20:55.000000 ./usr/share/man/man1/tor-print-ed-signing-cert.1.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     1012 2022-05-27 21:20:55.000000 ./usr/share/man/man1/tor-resolve.1.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)    54069 2022-05-27 21:20:55.000000 ./usr/share/man/man1/tor.1.gz
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      763 2022-05-27 21:20:55.000000 ./usr/share/man/man1/torify.1.gz
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/man/man5/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/man/man8/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)     1222 2022-05-27 21:20:55.000000 ./usr/share/man/man8/tor-instance-create.8.gz
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/runit/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/runit/meta/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/runit/meta/tor/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/runit/meta/tor/installed
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/tor/
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      351 2022-05-27 21:20:55.000000 ./usr/share/tor/tor-service-defaults-torrc
│ │ │ +-rw-r--r--   0 root         (0) root         (0)      431 2022-05-27 21:20:55.000000 ./usr/share/tor/tor-service-defaults-torrc-instances
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./var/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./var/log/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./var/log/runit/
│ │ │ +drwxr-xr-x   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./var/log/runit/tor/
│ │ │ +lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/log/supervise -> /run/runit/supervise/tor.log
│ │ │ +lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./etc/sv/tor/supervise -> /run/runit/supervise/tor
│ │ │ +lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/sbin/tor -> ../bin/tor
│ │ │ +lrwxrwxrwx   0 root         (0) root         (0)        0 2022-05-27 21:20:55.000000 ./usr/share/man/man5/torrc.5.gz -> ../man1/tor.1.gz
│ │ ├── ./usr/share/doc/tor/changelog.Debian.gz
│ │ │ ├── changelog.Debian
│ │ │ │ @@ -1,19 +1,19 @@
│ │ │ │ -tor (0.4.6.10-dev-20220509T161328Z-1~d11.bullseye+1) tor-nightly-0.4.6.x-bullseye; urgency=medium
│ │ │ │ +tor (0.4.6.10-dev-20220527T212004Z-1~d11.bullseye+1) tor-nightly-0.4.6.x-bullseye; urgency=medium
│ │ │ │  
│ │ │ │    * Build for tor-nightly-0.4.6.x-bullseye.
│ │ │ │  
│ │ │ │ - -- Peter Palfrader <build@runner-y9surhn-project-1218-concurrent-0>  Mon, 09 May 2022 16:14:16 +0000
│ │ │ │ + -- Peter Palfrader <build@bc1132b78dfc>  Fri, 27 May 2022 21:20:55 +0000
│ │ │ │  
│ │ │ │ -tor (0.4.6.10-dev-20220509T161328Z-1) tor-nightly-0.4.6.x; urgency=medium
│ │ │ │ +tor (0.4.6.10-dev-20220527T212004Z-1) tor-nightly-0.4.6.x; urgency=medium
│ │ │ │  
│ │ │ │ -  * Automated build of tor-nightly at 20220509T161328Z, git revision
│ │ │ │ +  * Automated build of tor-nightly at 20220527T212004Z, git revision
│ │ │ │      5568961cbf0749f5+4ba89c0ccc9452fa with debiantree e668385.
│ │ │ │  
│ │ │ │ - -- Peter Palfrader <build@runner-y9surhn-project-1218-concurrent-0>  Mon, 09 May 2022 16:14:12 +0000
│ │ │ │ + -- Peter Palfrader <build@bc1132b78dfc>  Fri, 27 May 2022 21:20:52 +0000
│ │ │ │  
│ │ │ │  tor (0.4.6.10-1) unstable; urgency=medium
│ │ │ │  
│ │ │ │    * New upstream version.
│ │ │ │  
│ │ │ │   -- Peter Palfrader <weasel@debian.org>  Sun, 27 Feb 2022 13:58:14 +0100
│ │ ├── ./usr/share/man/man8/tor-instance-create.8.gz
│ │ │ ├── tor-instance-create.8
│ │ │ │ @@ -1,17 +1,17 @@
│ │ │ │  '\" t
│ │ │ │  .\"     Title: tor-instance-create
│ │ │ │  .\"    Author: Peter Palfrader
│ │ │ │  .\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
│ │ │ │ -.\"      Date: 05/09/2022
│ │ │ │ +.\"      Date: 05/27/2022
│ │ │ │  .\"    Manual: Tor Manual
│ │ │ │  .\"    Source: Tor
│ │ │ │  .\"  Language: English
│ │ │ │  .\"
│ │ │ │ -.TH "TOR\-INSTANCE\-CREAT" "8" "05/09/2022" "Tor" "Tor Manual"
│ │ │ │ +.TH "TOR\-INSTANCE\-CREAT" "8" "05/27/2022" "Tor" "Tor Manual"
│ │ │ │  .\" -----------------------------------------------------------------
│ │ │ │  .\" * Define some portability stuff
│ │ │ │  .\" -----------------------------------------------------------------
│ │ │ │  .\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
│ │ │ │  .\" http://bugs.debian.org/507673
│ │ │ │  .\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
│ │ │ │  .\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```
