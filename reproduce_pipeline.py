#!/usr/bin/env python3

# Reproduces the build from a specific gitlab pipeline,
# and validates checksums of the built tor binaries.

import os
import requests
import shutil
import subprocess
import yaml

def get_pipeline(project_id, pipeline_id):
    req = requests.get(f'https://gitlab.torproject.org/api/v4/projects/{project_id}/pipelines/{pipeline_id}')
    return req.json()

def get_successful_job(project_id, pipeline_id, name):
    # TODO: handle paginating
    req = requests.get(f'https://gitlab.torproject.org/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs?per_page=100')
    print("looking for ", name)
    for j in req.json():
        #print(j)
        print(j['name'], j['status'])
    jobs = filter(lambda j: j['status'] == 'success' and j['name'] == name, req.json())
    # TODO: get most recent if multiple?
    return next(jobs)

def job2script(yml, job):
    full_script = []

    full_script.append('set -x')

    # Variables are set for before_script, script, and after_script. I think.
    variables = yml.get('variables', {})
    variables.update(job.get('variables', {}))
    for (k, v) in variables.items():
        full_script.append(f'export {k}="{v}"')

    # TODO: just mounting the same wd for now
    # Copy artifacts to input
#    full_script.push('if [ "`ls /artifacts`" != "" ]')
#    full_script.push('then')
#    full_script.push('  cp -r /artifacts/* .')
#    full_script.push('fi\n')

    for script_name in ['before_script', 'script', 'after_script']:
        # Run before_script in a subshell
        script = job.get(script_name)
        if script:
            full_script.append('(')
            full_script.extend(script)
            full_script.append(')')

    # TODO: just mounting the same wd for now
#    artifact_paths = script.get('artifacts', {}).get('paths', [])
#    for path in artifact_paths:
#        full_script.append(f'cp -r {path} /artifacts')

    return '\n'.join(full_script)

# TODO: we should get the image from the job, but then we need to handle variable substitution in the image name
def run_job(builddir, yml, job_name, image, ci_commit_branch, job_params):
    script = job2script(yml, yml[job_name])
    print(script)

    builddir = subprocess.check_output(['realpath', builddir], text=True).strip()

    env_args = [f'--env={key}={value}' for (key, value) in job_params.items()]

    p = subprocess.Popen([
        'docker',
        'run',
        '-i',
        f'--env=CI_COMMIT_BRANCH={ci_commit_branch}',
        # XXX I don't see a more precise way to get this, but I *think* this
        # is correct for these jobs.
        f'--env=CI_COMMIT_TAG={ci_commit_branch}',
        f'--env=CI_JOB_NAME={job_name}',
        f'--env=CI_PROJECT_DIR=/build',
        ] + [f'--env={key}={value}' for (key, value) in job_params.items()] + [
        '--rm',
        f'--volume={builddir}:/build',
        '--workdir=/build',
        image,
        '/bin/sh'
        ],
        text=True,
        stdin=subprocess.PIPE)
    p.stdin.write(script)
    p.stdin.close()
    p.wait()
    assert(p.returncode == 0)

def checkout_build_repo(url=None, branch=None, ref=None, dst_dir=None):
    # `pristine-tar` needs a non-shallow clone
    subprocess.check_call([
        'git',
        'clone',
        f'--branch={branch}',
        url,
        dst_dir,
        ])
    subprocess.check_call([
        'git',
        'checkout',
        ref,
        ],
        cwd=dst_dir)

def download_artifacts(project_id, job_id, artifact_dir):
    artifacts = requests.get(f'https://gitlab.torproject.org/api/v4/projects/{project_id}/jobs/{job_id}/artifacts')
    f = open(f'{artifact_dir}/artifacts.zip', 'wb')
    f.write(artifacts.content)
    f.close()
    subprocess.check_call('unzip artifacts.zip', shell=True, cwd=artifact_dir)
    subprocess.check_call('rm artifacts.zip', shell=True, cwd=artifact_dir)

def reproduce(pipeline_id, job_params):
    # Project id for https://gitlab.torproject.org/tpo/core/debian/tor.git
    project_id=1218
    pipeline = get_pipeline(project_id, pipeline_id)
    print(pipeline)

    # Where we'll put our build output.
    build_dir=f'reproduce-{pipeline_id}'

    pipeline_src_dir=f'{build_dir}/pipeline_src'

    their_build_source = get_successful_job(project_id, pipeline_id, 'build_source-release')


    # Field values are used in the order defined in `.matrix_full` in the original yml.
    # `arm64v8` uses a slightly different ordering than the rest.
    # TODO: dynamically look for the matching job in the yml to get the right ordering.
    if job_params["DOCKER_ARCH"] == 'arm64v8':
        job_matrix_string = ', '.join([
                job_params["OS"],
                job_params["DOCKER_ARCH"],
                job_params["SUITE"],
                job_params["RUNNER_TAG"],
                job_params["HOSTING_PROVIDER_TAG"],
                job_params["IMAGE_EXTENSION"],
                job_params["SPECIAL"]])
    else:
        job_matrix_string = ', '.join([
                job_params["OS"],
                job_params["SUITE"],
                job_params["DOCKER_ARCH"],
                job_params["RUNNER_TAG"],
                job_params["HOSTING_PROVIDER_TAG"],
                job_params["IMAGE_EXTENSION"],
                job_params["SPECIAL"]])


    their_build_binary = get_successful_job(project_id, pipeline_id, f'build_binary-release: [{job_matrix_string}]')
    pipeline_branch=their_build_source['ref']
    commit=their_build_source['commit']['id']
    subprocess.check_call(f'mkdir -p {build_dir}/theirs', shell=True)
    download_artifacts(project_id, their_build_source['id'], f'{build_dir}/theirs')
    download_artifacts(project_id, their_build_binary['id'], f'{build_dir}/theirs')

    checkout_build_repo(
            url='https://gitlab.torproject.org/tpo/core/debian/tor.git',
            branch=pipeline_branch,
            ref=commit,
            dst_dir=pipeline_src_dir)

    # Variables from https://gitlab.torproject.org/tpo/core/debian/tor/-/settings/ci_cd.
    #
    # Private keys intentionally omitted; we don't need or want them.
    #
    # "File" type variables must be copied into the build directory, and the env var
    # set to the path of that file
    shutil.copy('TOR_DEBIAN_RELEASE_KEYRING_DEBIAN', pipeline_src_dir)
    shutil.copy('TOR_DEBIAN_RELEASE_KEYRING_UPSTREAM', pipeline_src_dir)
    pipeline_variables = {
            'TOR_DEBIAN_RELEASE_KEYRING_DEBIAN': '/build/TOR_DEBIAN_RELEASE_KEYRING_DEBIAN',
            'TOR_DEBIAN_RELEASE_KEYRING_UPSTREAM': '/build/TOR_DEBIAN_RELEASE_KEYRING_UPSTREAM',
            }

    ci_yaml = yaml.unsafe_load(open(f'{pipeline_src_dir}/debian/.debian-ci.yml'))
    run_job(pipeline_src_dir, ci_yaml, 'build_source-release', 'debian:stable-slim', pipeline_branch, pipeline_variables)
    job_vars = {}
    job_vars.update(job_params)
    job_vars.update(pipeline_variables)
    run_job(pipeline_src_dir, ci_yaml, 'build_binary-release', f'{job_params["OS"]}:{job_params["SUITE"]}{job_params["IMAGE_EXTENSION"]}', pipeline_branch, job_vars)

    subprocess.check_call(f'mkdir {build_dir}/ours', shell=True)
    subprocess.check_call(f'cp -r {pipeline_src_dir}/source-packages {build_dir}/ours/', shell=True)
    subprocess.check_call(f'cp -r {pipeline_src_dir}/binary-packages {build_dir}/ours/', shell=True)

    print("Done!")
    print(f"Our artifacts: {build_dir}/ours")
    print(f"Their artifacts: {build_dir}/theirs")
    print("Try diffoscope to compare; e.g.:")
    print(f"diffoscope {build_dir}/theirs/binary-packages/*/tor_*.deb {build_dir}/ours/binary-packages/*/tor_*.deb")
    print(
    """
    You may also want to validate that the package on deb.torproject.org matches the one in the artifacts.
    You can grab the corresponding deb from https://deb.torproject.org/torproject.org/pool/main/t/tor/ (it'll have
    the same base name as in downloaded artifact), or just cross-check the hash from e.g.
    https://deb.torproject.org/torproject.org/dists/bullseye/main/binary-amd64/Packages
    """)

if __name__ == '__main__':
    # See `matrix_full` at
    # https://gitlab.torproject.org/tpo/core/debian/tor/-/blob/debian-main/debian/.debian-ci.yml#L154
    job_params = {
            'OS': 'debian',
            'SUITE': 'bullseye',
            'DOCKER_ARCH': 'amd64',
            'RUNNER_TAG': 'amd64',
            'HOSTING_PROVIDER_TAG': 'tpa',
            'IMAGE_EXTENSION': '-slim',
            'SPECIAL':''}

    # Example alt config:
    # job_params = {
    #         'OS': 'ubuntu',
    #         'SUITE': 'xenial',
    #         'DOCKER_ARCH': 'amd64',
    #         'RUNNER_TAG': 'amd64',
    #         'HOSTING_PROVIDER_TAG': 'tpa',
    #         'IMAGE_EXTENSION': '',
    #         'SPECIAL':''}

    # Another alt config, this one for arm. Should only
    # run this on an aarch64 host machine (or emulator).
    # job_params = {
    #         'OS': 'debian',
    #         'SUITE': 'bullseye',
    #         'DOCKER_ARCH': 'arm64v8',
    #         'RUNNER_TAG': 'aarch64',
    #         'HOSTING_PROVIDER_TAG': 'osuosl',
    #         'IMAGE_EXTENSION': '-slim',
    #         'SPECIAL':''}

    # Current most-recent stable build - 0.4.7.13.
    # Must be a pipeline that ran on a tag - look in
    # https://gitlab.torproject.org/tpo/core/debian/tor/-/pipelines?scope=tags&page=1
    reproduce(62395, job_params)

